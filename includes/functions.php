<?php

function listFolderFiles($dir){
    $ffs = scandir($dir);

    unset($ffs[array_search('.', $ffs, true)]);
    unset($ffs[array_search('..', $ffs, true)]);
    unset($ffs[array_search('.git', $ffs, true)]);
    unset($ffs[array_search('.DS_Store', $ffs, true)]);

    // prevent empty ordered elements
    if (count($ffs) < 1)
        return;

    foreach($ffs as $ff) {
	$link = $_SERVER['PHP_SELF'] . '?filename=' . urlencode(str_replace($_SERVER['DOCUMENT_ROOT'], '', $dir) . '/' . $ff);

	if(is_dir($dir.'/'.$ff)) {
		$li_class = "Node";
	} else {
		$li_class = "Leaf";
	}

	echo '<li>
		<span class="stm-icon"></span>';
		if(is_dir($dir.'/'.$ff)) {
			echo '<span class="stm-content">'.$ff.'</span>';
		} else {
			echo '<a class="stm-content"  href="'. $link .'" >' . $ff . '</a>';
		}
		if(is_dir($dir.'/'.$ff)) {
			echo '<ul>';
				listFolderFiles($dir.'/'.$ff);
			echo '</ul>';
		}
        echo '</li>';
    }
}

function ReadFileContent() {
	if (isset($_GET['filename'])) {
		$count = 0;
		$line_count = 0;
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . urldecode($_GET['filename']);

		$file = fopen($filename,"r");

		while(!feof($file)) {
		    $line_count++;
		    fgets($file);
		}
		fclose($file);

		$file = fopen($filename,"r");
		$digits_count = strlen($line_count);

		echo "<pre>";
		while(!feof($file)) {
			$count++;
			$current_digit_count = strlen($count);
    			$diff_digits_count = $digits_count - $current_digit_count;
			echo "<code>".htmlspecialchars(fgets($file)). "</code>";
 		}
		echo "</pre>";

		fclose($file);
	}
}


function GenerateLineNumber($diff_digits_count, $count) {
    $line_number = "";
    for ($i =0; $i < $diff_digits_count; $i++) {
	$line_number .= "0";
    }
    
    $line_number .= $count;
    
    return $line_number;
}

function IframeResult() {
    if (isset($_GET['filename'])) {
	echo "<iframe src='" . $_GET['filename'] . "' height='800px'></iframe>";
    }
}

?>